# Euro Truck Simulator 2
## UI
- [Route Advisor](https://forum.scssoft.com/viewtopic.php?f=202&t=234726)
  - 6.03
  - February 27th, 2022
  - sha256
    - Route_Advisor_KAAC_GSM_Center_Top_1LTT_NF_MBB_TBB_v6.03.scs - `9ad4e46d6626d896a2fc716949dc950b88c441c398b499b18c67ff7ceec85622`
    - Icons_for_ETS2.scs - `c050617fcc5c46de8ab7cd55889dfb031e4cf45ddb196501257b19d529bd60ee`
- [SiSL's FlatUI](https://steamcommunity.com/sharedfiles/filedetails/?id=690213540)
  - Steam Workshop

## Trailers
- [Baobab Tree](https://steamcommunity.com/sharedfiles/filedetails/?id=734984242)
  - Steam Workshop
- [SiSL's Trailer Pack](https://steamcommunity.com/sharedfiles/filedetails/?id=842257514)
  - Steam Workshop

## Models & textures
- [Real Airline Mod](http://ets2.lt/en/real-airline-mod/)
  - October 31st, 2015
  - sha256 - `4aa93f95f36c62366bcb8b705b0464efa8b29953df612635ee0e1aa852605079`
- [Hapag Lloyd Container Ship](http://forum.scssoft.com/viewtopic.php?f=33&t=88174)
  - August 24, 2017
  - sha256 - `1825716f166a929317a3729c5e27e9be4fc1f1bfa59fd08baae736617ca8964a`

## Truck customization
- [Volvo FH16 2009 HD Gauges](https://steamcommunity.com/sharedfiles/filedetails/?id=616609355)
  - Steam Workshop
- [FH16 Exclusive Interior Color Change](FH16 Exclusive Interior Color Change.scs)
  - custom
- [SiSL's Mega Pack](https://steamcommunity.com/sharedfiles/filedetails/?id=645553604)
  - Steam Workshop
- [DP Speditions Skin](DP Speditions.scs)
  - custom
