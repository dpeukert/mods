# X-Plane mods

## Aircraft
| Name                                                              | Version | Date       | Notes                                                        | Source                                                                                                                                        |
| :---------------------------------------------------------------- | ------: | :--------- | :----------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------- |
| Antonov An-148                                                    |     1.1 | 2010/11/22 | X-Plane 11 aircraft, unofficial X-Plane 11 patch required    | https://forums.x-plane.org/index.php?/files/file/11584-an-148-airliner-with-3d-cockpit/                                                       |
| Antonov An-148 - unofficial X11 patch                             |   1.0.0 | 2017/05/10 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/37667-an-148-x-plane-11-compatability-patch-unofficial/                                      |
| ATR 42-500                                                        |   1.0.5 | 2021/10/26 | X-Plane 11 aircraft, 1.0.5 patch included in a separate file | https://forums.x-plane.org/index.php?/files/file/52797-atr-42-500/                                                                            |
| ATR 72-500                                                        |   1.0.4 | 2021/10/09 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/52748-atr_72-500/                                                                            |
| Avro Vulcan B.2                                                   |     1.0 | 2023/01/17 |                                                              | https://forums.x-plane.org/index.php?/files/file/84782-avro-vulcan-bmk2-for-v12/                                                              |
| B-52G NASA                                                        |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| Bede BD-5J                                                        |   3.4.1 | 2023/11/06 |                                                              | https://forums.x-plane.org/index.php?/files/file/27269-bd-5j-microjet/                                                                        |
| Beriev A-50                                                       |     1.1 | 2019/07/09 | X-Plane 11 aircraft, use version with no KLN                 | https://forums.x-plane.org/index.php?/files/file/53395-beriev-a-50/                                                                           |
| Boeing B-17G                                                      |  11.41f | 2020/08/09 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/38403-boeing-b-17g-v141-xplane-11-khamsin-studio/                                            |
| Boeing B747-400                                                   |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| C-130                                                             |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| Chance Vought F4U-1 Corsair                                       |     2.1 | 2023/06/14 |                                                              | https://forums.x-plane.org/index.php?/files/file/83056-chance-vought-red-tag-f4u-1-corsair/                                                   |
| Columbia 400                                                      |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| Douglas C-47 Dakota                                               | 3.12.12 | 2023/10/22 |                                                              |                                                                                                                                               |
| Handley Page Victor B.2                                           |     2.3 | 2021/12/04 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/50378-handley-page-victor-b-mk2/                                                             |
| Ilyushin Il-76TD                                                  |    1.36 | 2020/07/07 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/52419-ilyushin-il-76td/                                                                      |
| Ilyushin Il-76TD - liveries                                       |         | 2013/10/05 | only use for liveries                                        | https://www.avsimrus.com/f/x-plane-aircrafts-95/il-76-51516.html                                                                              |
| Let L-410 Turbolet                                                |   1.2.2 | 2023/07/18 |                                                              | https://forums.x-plane.org/index.php?/files/file/88175-let-l-410-turbolet-xp12/                                                               |
| LISA Akoya                                                        |     2.4 | 2023/12/05 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/35322-lisa-akoya-by-aerobask/                                                                |
| Lockheed P-38 Lightning                                           |     1.0 | 2023/02/24 |                                                              | https://forums.x-plane.org/index.php?/files/file/85323-lockheed-p-38-lightning-for-v12/                                                       |
| Lockheed SR-71                                                    |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| McDonnell Douglas KC-10                                           |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| Messerschmitt Bf 109E-4                                           |   1.3.0 | 2022/11/01 |                                                              | https://forums.x-plane.org/index.php?/files/file/83091-messerschmitt-red-tag-bf-109-e4/                                                       |
| Mikoyan-Gurevich MiG-15                                           |    2301 | 2023/02/07 | Yak-3 and MiG-17 included                                    | https://store.x-plane.org/MiG-15_p_225.html                                                                                                   |
| Mikoyan-Gurevich MiG-15 - Cuban Revolutionary Air Force 32 livery |   1.0.0 | 2020/04/21 |                                                              | https://forums.x-plane.org/index.php?/files/file/60802-cuban-revolutionary-air-force-paint-for-the-mladg-mig-15-available-from-the-org-store/ |
| Mikoyan-Gurevich MiG-15 - Polish Air Force 712 livery             |   1.0.0 | 2020/04/22 |                                                              | https://forums.x-plane.org/index.php?/files/file/60861-polish-air-force-712-paint-for-the-mladg-mig-15-available-from-the-org-store/          |
| Mitsubishi A6M Zero Bomber                                        |   1.3.0 | 2023/09/13 |                                                              | https://forums.x-plane.org/index.php?/files/file/54136-mitsubishi-a6m-zero-bomber/                                                            |
| Mitsubishi A6M Zero Fighter                                       |   1.3.0 | 2023/09/13 |                                                              | https://forums.x-plane.org/index.php?/files/file/54134-mitsubishi-a6m-zero-fighter/                                                           |
| North American T-6G Texan                                         |  11.41i | 2020/04/18 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/59299-north-american-t-6g-texan-khamsin-studio/                                              |
| Piper PA-18 KingCub STC                                           |     2.1 | 2023/08/25 |                                                              | https://forums.x-plane.org/index.php?/files/file/87644-lr-kingcub-stc/                                                                        |
| X-15                                                              |         |            |                                                              | copied from X-Plane 11                                                                                                                        |
| Yakovlev Yak-18T                                                  |     1.7 | 2023/01/11 |                                                              | https://forums.x-plane.org/index.php?/files/file/51672-pwdt-yakovlev-yak-18t/                                                                 |
| Yakovlev Yak-55M                                                  |   1.2.0 | 2023/07/15 |                                                              | https://forums.x-plane.org/index.php?/files/file/84575-yak-55m/                                                                               |
| Zivko Edge 540                                                    |   1.0.0 | 2019/12/28 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/55469-zivko-edge-540-classic/                                                                |
| Zlin Z-142                                                        |     1.4 | 2022/09/27 | X-Plane 11 aircraft                                          | https://forums.x-plane.org/index.php?/files/file/58226-pwdtnhadrian-zlin-z-142/                                                               |

## Sceneries - [scenery_packs.ini](scenery_packs.ini)
| Name                              | Version | Date       | Dependencies                                                               | Source                                                                                                |
| :-------------------------------- | ------: | :--------- | :------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------- |
| **AIRPORTS**                      |         |            |                                                                            |                                                                                                       |
| EBBR - Brussels (Aerosoft)        |         |            |                                                                            | copied from X-Plane 10                                                                                |
| EDDF - Frankfurt (Aerosoft)       |         |            |                                                                            | copied from X-Plane 11                                                                                |
| EGBB - Birmingham (Aerosoft)      |         |            |                                                                            | copied from X-Plane 10                                                                                |
| EGCC - Manchester (Aerosoft)      |         |            |                                                                            | https://www.aerosoft.com/en/account/downloads                                                         |
| EGKK - London-Gatwick (Aerosoft)  |         |            |                                                                            | copied from X-Plane 10                                                                                |
| EGLL - Heathrow (Aerosoft)        |         |            |                                                                            | copied from X-Plane 11                                                                                |
| EGPF - Glasgow (Aerosoft)         |         |            |                                                                            | copied from X-Plane 10                                                                                |
| EGSS - London-Stansted (Aerosoft) |         |            |                                                                            | copied from X-Plane 10                                                                                |
| Kahoolawe Heliport                |       2 |            |                                                                            | https://hawaii-photoreal.com/xp/kahoolawe-v2/                                                         |
| LEBL - Barcelona (Aerosoft)       |         |            |                                                                            | copied from X-Plane 10                                                                                |
| LEMD - Madrid (Aerosoft)          |         |            |                                                                            | copied from X-Plane 10                                                                                |
| LFPG - Paris CDG (Aerosoft)       |         |            |                                                                            | copied from X-Plane 10                                                                                |
| LFPO - Paris Orly (Aerosoft)      |         |            |                                                                            | copied from X-Plane 11                                                                                |
| LKKL - Kladno                     |     1.0 | 2015/01/10 | OpenSceneryX, R2_Library                                                   | https://www.flightsim.cz/fsitem.php?act=exe&id=766                                                    |
| LKMT - Ostrava Mosnov             |  beta 1 | 2020/03/13 | AutoGate, Flags of the World, Handy Objects, MisterX Library, OpenSceneryX | https://www.flightsim.cz/download/lkmt-ostrava-mosnov-xplane.html                                     |
| LKPR - Prague - Vaclav Havel      |    1.01 | 2019/11/30 | MisterX Library, OpenSceneryX                                              | no longer available, sha256 - `58fe9be258e2122e215e304251a472d5492a4626ece74c72452ec0d1e6a1de48`      |
| LKSZ - Sazena                     |         | 2014/07/08 | OpenSceneryX, R2_Library, RuScenery                                        | https://www.flightsim.cz/fsitem.php?act=exe&id=731                                                    |
| LSGG - Genf (Aerosoft)            |         |            |                                                                            | copied from X-Plane 10                                                                                |
| PHMK - Molokai                    |       2 |            |                                                                            | https://hawaii-photoreal.com/xp/phmk-v2/                                                              |
| PHNY - Lanai                      |       2 |            |                                                                            | https://hawaii-photoreal.com/xp/phny-v2/                                                              |
| **SPECIFIC OBJECTS**              |         |            |                                                                            |                                                                                                       |
| Fakultni nemocnice Brno           |         | 2014/12/24 | OpenSceneryX, R2_Library                                                   | https://www.flightsim.cz/fsitem.php?act=exe&id=757                                                    |
| Fakultni nemocnice Praha Motol    |         | 2018/12/16 | OpenSceneryX, R2_Library                                                   | https://www.flightsim.cz/fsitem.php?act=exe&id=832                                                    |
| Hrady a zamky (leguan)            |       4 | 2020/02/27 | OpenSceneryX, R2_Library, RuScenery, xp11_airport_scenery_compat           | https://www.flightsim.cz/fsitem.php?act=exe&id=833                                                    |
| Hrady a zamky (ostatni)           |         | 2016/02/21 | xp11_airport_scenery_compat                                                | https://www.flightsim.cz/fsitem.php?act=exe&id=807                                                    |
| Hrady a zamky I                   |         | 2018/10/31 | OpenSceneryX                                                               | https://www.flightsim.cz/fsitem.php?act=exe&id=821                                                    |
| Hrady a zamky II                  |         | 2018/10/31 |                                                                            | https://www.flightsim.cz/fsitem.php?act=exe&id=822                                                    |
| Lomy                              |     1.0 | 2016/02/21 | OpenSceneryX, R2_Library, RuScenery                                        | https://www.flightsim.cz/fsitem.php?act=exe&id=802                                                    |
| Obilna sila CR a SR               |       2 | 2018/12/16 | R2_Library, RuScenery                                                      | https://www.flightsim.cz/fsitem.php?act=exe&id=830                                                    |
| OSM objekty CR+SR                 | 2019.11 | 2019/11/01 | OpenSceneryX, R2_Library                                                   | https://www.flightsim.cz/fsitem.php?act=exe&id=841                                                    |
| Prumyslove objekty                |     3.1 | 2020/04/02 | RuScenery                                                                  | https://www.flightsim.cz/fsitem.php?act=exe&id=831                                                    |
| Vysilace                          |     1.0 | 2020/04/02 |                                                                            | https://www.flightsim.cz/fsitem.php?act=exe&id=852                                                    |
| **OVERLAYS**                      |         |            |                                                                            |                                                                                                       |
| **MESHES**                        |         |            |                                                                            |                                                                                                       |
| Hawaii Photoreal                  |     2.1 |            |                                                                            | https://hawaii-photoreal.com/Downloads-X-Plane11                                                      |
| Ortofoto CR a SR ZL17             |         |            |                                                                            | http://xpl.cz/wiki/Ortofoto#T.C3.A9m.C4.9B.C5.99_kompletn.C3.AD_ortofoto_.C4.8CR_a_SR_v_ZL17          |
| **LIBRARIES**                     |         |            |                                                                            |                                                                                                       |
| Flags of the World                |       3 | 2013/11/19 |                                                                            | https://forums.x-plane.org/index.php?/files/file/17090-flags-of-the-world-real-flag-ii                |
| Handy Objects                     |    8.30 | 2023/08/07 |                                                                            | https://forums.x-plane.org/index.php?/files/file/24261-the-handy-objects-library                      |
| MisterX Library                   |    2.0c | 2023/05/06 |                                                                            | https://forums.x-plane.org/index.php?/files/file/28167-misterx-library-and-static-aircraft-extension/ |
| OpenSceneryX                      |   4.4.0 | 2020/03/28 | Sandy Barbour's CustomSBDatarefs004                                        | https://www.opensceneryx.com/                                                                         |
| R2_Library                        |       3 | 2020/09/28 |                                                                            | http://r2.xpl.cz/                                                                                     |
| RuScenery                         |   2.1.6 | 2011/11/26 |                                                                            | https://ruscenery.x-air.ru/                                                                           |
| xp11_airport_scenery_compat       |         |            |                                                                            | com_Const_Trailer.obj and all related files copied from X-Plane 11                                    |

## Utilities & plugins
| Name                                | Version | Date       | Source                                                                                                                        |
| :---------------------------------- | ------: | :--------- | :---------------------------------------------------------------------------------------------------------------------------- |
| Airport Navigator                   |   1.5.7 | 2022/11/20 | https://forums.x-plane.org/index.php?/files/file/34155-airport-navigator/                                                     |
| AutoGate plugin for XP12            |    1.81 | 2023/01/25 | https://forums.x-plane.org/index.php?/files/file/84268-autogate-plugin-for-xp12                                               |
| FlyWithLua NG+                      |  2.8.10 | 2023/08/28 | https://forums.x-plane.org/index.php?/files/file/82888-flywithlua-ng-next-generation-plus-edition-for-x-plane-12-win-lin-mac/ |
| Sandy Barbour's CustomSBDatarefs004 |    1.10 |            | http://www.xpluginsdk.org/misc_plugins.htm                                                                                    |
| Slew Mode                           |    1.05 | 2013/02/12 | https://forums.x-plane.org/index.php?/files/file/15506-slew-mode-plugin/                                                      |
| X-ReloadEd                          |   1.1b1 | 2017/11/28 | https://forums.x-plane.org/index.php?/files/file/18878-x-reloaded-linmacwin3264/                                              |

# Check out

## Planes
- http://www.avsimrus.com/f/x-plane-aircrafts-95/il-96-400-by-ramzess-airplane-adaptation-for-x-plane-11-62169.html
  - https://forums.x-plane.org/index.php?/files/file/51761-il96-400-ramzess-new-liveries-russian-air-force-rossiya-cubana-vaso/
- https://forums.x-plane.org/index.php?/files/file/18661-eclipse-550/
- https://forums.x-plane.org/index.php?/files/file/29900-aero-boero-ab-115/
- https://forums.x-plane.org/index.php?/files/file/36862-transall-c-160/
- https://forums.x-plane.org/index.php?/files/file/36952-mb339a_pan_pony10/
- https://forums.x-plane.org/index.php?/files/file/37010-gulfstream-g-iv-sp-v11/
- https://forums.x-plane.org/index.php?/files/file/37204-mooney-encore/
- https://forums.x-plane.org/index.php?/files/file/37339-globe-super-swift/
- https://forums.x-plane.org/index.php?/files/file/37649-rafale-c-bomber/
- https://forums.x-plane.org/index.php?/files/file/37651-rafale-c-solo-display/
- https://forums.x-plane.org/index.php?/files/file/37800-alphajet-type-a/
- https://forums.x-plane.org/index.php?/files/file/37801-alphajet-type-e/
- https://forums.x-plane.org/index.php?/files/file/38058-pottier-130-ul/
- https://forums.x-plane.org/index.php?/files/file/38316-consolidated-pby-5a-catalina-9767/
- https://forums.x-plane.org/index.php?/files/file/38418-eco-ercoupe-415c/
- https://forums.x-plane.org/index.php?/files/file/38438-glasair-iii-deluxe-package/
- https://forums.x-plane.org/index.php?/files/file/38463-sepecat-jaguar-version-045/
- https://forums.x-plane.org/index.php?/files/file/39189-lockheed-l-749-constellation/
- https://forums.x-plane.org/index.php?/files/file/39843-mcdonnell-douglas-av-8b-harrier-ii-usmc/
- https://forums.x-plane.org/index.php?/files/file/40017-dewoitine-d520/
- https://forums.x-plane.org/index.php?/files/file/41357-lancair-legacy-fg/
- https://forums.x-plane.org/index.php?/files/file/41882-dash-7-150-for-x-plane-11-ver/
- https://forums.x-plane.org/index.php?/files/file/43052-mg-dornier-do-328/
- https://forums.x-plane.org/index.php?/files/file/44466-fokker-50/
- https://forums.x-plane.org/index.php?/files/file/44529-antonov-an-2-ksgy/
- https://forums.x-plane.org/index.php?/files/file/44642-focke-wulf-ta-152-h1/
- https://forums.x-plane.org/index.php?/files/file/45373-aerobask-robin-dr401-cdi-155/
- https://forums.x-plane.org/index.php?/files/file/45966-hawker-hurricane/
- https://forums.x-plane.org/index.php?/files/file/47097-supermarine-spitfire-mk-ix/
  - https://forums.x-plane.org/index.php?/files/file/51201-supermarine-spitfire-f-mk-ix-m63-s-update/
- https://forums.x-plane.org/index.php?/files/file/47128-lockheed-c-5b-galaxy/
- https://forums.x-plane.org/index.php?/files/file/47677-lockheed-p-38-lightning/
  - https://forums.x-plane.org/index.php?/files/file/51618-lockheed-p-38-lightning-update/
- https://forums.x-plane.org/index.php?/files/file/47996-joint-strike-fighter/
- https://forums.x-plane.org/index.php?/files/file/48223-vflyteair-ryan-navion-205-vintage-freeware/
- https://forums.x-plane.org/index.php?/files/file/48859-mig-29kr/
- https://forums.x-plane.org/index.php?/files/file/50191-douglas-dc-9-32/
- https://forums.x-plane.org/index.php?/files/file/51843-lockheed-l-749-constellation-star-of-maryland/
- https://forums.x-plane.org/index.php?/files/file/53416-xpfr-dr400-140b-xp11/
- https://forums.x-plane.org/index.php?/files/file/54444-fairchild-packet-c-119-flying-boxcar/
- https://forums.x-plane.org/index.php?/files/file/55879-saab-jas-39e-gripen/
- https://forums.x-plane.org/index.php?/files/file/56924-caravelle-se210/
- https://forums.x-plane.org/index.php?/files/file/57980-de-havilland-vampire-fb-mk5/
- https://forums.x-plane.org/index.php?/files/file/60413-aerobask-fokker-dr1/
- https://forums.x-plane.org/index.php?/files/file/60975-lockheed-f-117-nighthawk/
- https://forums.x-plane.org/index.php?/files/file/69672-mig-23-and-mig-27-floggers/
- https://forums.x-plane.org/index.php?/files/file/83250-tupolev-tu-154-by-felis/
- https://forums.x-plane.org/index.php?/files/file/83298-beechcraft-bonanza-a36/
- https://forums.x-plane.org/index.php?/files/file/83623-messerschmitt-red-tag-me-262-schwalbe/
- https://forums.x-plane.org/index.php?/files/file/84653-rockwell-b-1b-lancer-for-v12/
- https://forums.x-plane.org/index.php?/files/file/85256-atr-42-500-rivi%C3%A8re/
- https://forums.x-plane.org/index.php?/files/file/85921-zero-mitsubishi-a6m-fighter-for-x-plane-12/
- https://forums.x-plane.org/index.php?/files/file/85983-lockheed-p-80-shooting-star-for-v12/
- https://forums.x-plane.org/index.php?/files/file/86322-virtavia-c-17-xp12/
- https://forums.x-plane.org/index.php?/files/file/86387-virtavia-b-29-superfortress-xp12/
- https://forums.x-plane.org/index.php?/files/file/86450-p-51-mustang-xp12/
- https://forums.x-plane.org/index.php?/files/file/86850-aeronautica-macchi-red-tag-m-67-schneider-trophy-1929/
- https://forums.x-plane.org/index.php?/files/file/86987-northrop-b-2-xp12/
- https://forums.x-plane.org/index.php?/files/file/88634-mig-23-mld/
- https://forums.x-plane.org/index.php?/files/file/89283-tu-160-m2/
- https://github.com/parshukov/An24-Felis-for-XP11
  - https://forums.x-plane.org/index.php?/files/file/54511-hotfixes-for-felis-antonov-an-24rv-1135/
- https://github.com/parshukov/Yak-40-Felis-for-X-Plane-11
- https://store.x-plane.org/Let-L-200D-Morava_p_1461.html

## Sceneries
- https://forums.x-plane.org/index.php?/files/file/16007-eglc-canary-wharf/
- https://forums.x-plane.org/index.php?/files/file/21953-london-buildings-updated-3-18/
- https://forums.x-plane.org/index.php?/files/file/32684-norway-pro-vfr-scenery/
- https://forums.x-plane.org/index.php?/files/file/34232-gb-pro-v2-detailed-vfr-scenery-for-great-britain/
- https://forums.x-plane.org/index.php?/files/file/35172-airport-environment-hd/
- https://forums.x-plane.org/index.php?/files/file/37443-new-zealand-south-island-small-airport-package-by-hapet/
  - https://www.youtube.com/watch?v=y32TGcnGq1A
- https://forums.x-plane.org/index.php?/files/file/43626-vhhh-hong-kong-chek-lap-kok-international/
- https://forums.x-plane.org/index.php?/files/file/49095-japan-pro/
- https://forums.x-plane.org/index.php?/files/file/49211-welcome-to-pripyat-public-beta/
- https://forums.x-plane.org/index.php?/files/file/64101-vhhx-kai-tak-hong-kong-international/
- https://mildlyalert.wordpress.com/lyndimans-new-zealand-ortho-photography-set/
  - https://mildlyalert.wordpress.com/2020/06/12/freeware-osm-overlay-for-new-zealand-and-the-chatham-islands/
- https://propstrikestudio.com/products/fisheries
- https://simheaven.com/xp12-sceneries/
- https://www.flightsim.cz/download/ceske-budejovice-xplane.html
- https://www.flightsim.cz/fsitem.php?act=exe&id=803 - Praha
- https://www.flightsim.cz/fsitem.php?act=exe&id=834 - Cesky raj
